public class Triangle {

        public static double side1, side2, side3;
        public static int equilateral=0, isosceles=0, rectangular=0, arbitrary=0;
        public double square;
        public Triangle (int side1, int side2, int side3){
            this.side1=side1;
            this.side2=side2;
            this.side3=side3;
        }
        public double square (){
            double pper = (side1+side2+side3)/2;
            square = Math.sqrt(pper*(pper-side1)*(pper-side2)*(pper-side3));
            return square;
        }
        public double perimeter (){
            double perimeter = side1 + side2 + side3;
            return perimeter;
        }
        public  void types_of_rect(){

    if(side1==side2 || side2==side3 || side1==side3) {
        if (side1 == side2 && side1 == side3) {
            equilateral++;
        }
        else{
            isosceles++;
        }
    }
    double a=side1;
    double b=side2;
    double c=side3;
    if ((a * a + b * b == c * c) || (a * a + c * c == b * b) || (c * c + b * b == a * a)){
        rectangular++;
    }

        }
    }