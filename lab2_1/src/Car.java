import java.util.Scanner;

public class Car {


    public int id, year, price, registration;
    public String marka, model, color;
    public Car(int id, String marka, String model, int year, String color, int price, int registration){
        this.id=id;
        this.marka=marka;
        this.model=model;
        this.year=year;
        this.color=color;
        this.price=price;
        this.registration=registration;
    }
    public int getId() {
        return this.id;
    }
    public void setId(int id) {
        this.id=id;
    }
    public String getMarka() {
        return this.marka;
    }

    public void setMarka(String marka) {
        this.marka=marka;
    }
    public String getModel() {
        return this.model;
    }
    public void setModel(String model) {
        this.model=model;
    }
    public int getYear() {
        return this.year;
    }
    public void setYear(int year) {
        this.year=year;
    }
    public String getColor() {
        return this.color;
    }

    public void setColor(String color) {
        this.color=color;
    }
    public int getPrice() {
        return this.price;
    }
    public void setPrice(int price) {
        this.price=price;
    }
    public int getRegistration() {
        return this.registration;
    }
    public void setRegistrarion(int registrarion) {
        this.registration=registration;
    }

}
